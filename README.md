# Employee

This program allows a user to insert a new employee to a database, edit an existing employee's information, search for one or more employees using certain parameters, and delete an employee from the database.  A video demonstration is included in the file Employee.mkv.

The password required to use the access the database has been removed, so the application will not function in this state.
