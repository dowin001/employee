﻿
namespace Employee_Form
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IDText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.confirmDeleteButton = new System.Windows.Forms.Button();
            this.compareID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // IDText
            // 
            this.IDText.Location = new System.Drawing.Point(104, 73);
            this.IDText.Name = "IDText";
            this.IDText.Size = new System.Drawing.Size(100, 20);
            this.IDText.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter the selected employee\'s ID number to confirm deletion.";
            // 
            // confirmDeleteButton
            // 
            this.confirmDeleteButton.Location = new System.Drawing.Point(116, 99);
            this.confirmDeleteButton.Name = "confirmDeleteButton";
            this.confirmDeleteButton.Size = new System.Drawing.Size(75, 23);
            this.confirmDeleteButton.TabIndex = 2;
            this.confirmDeleteButton.Text = "Delete";
            this.confirmDeleteButton.UseVisualStyleBackColor = true;
            this.confirmDeleteButton.Click += new System.EventHandler(this.confirmDeleteButton_Click);
            // 
            // compareID
            // 
            this.compareID.Location = new System.Drawing.Point(104, 12);
            this.compareID.Name = "compareID";
            this.compareID.Size = new System.Drawing.Size(100, 20);
            this.compareID.TabIndex = 3;
            this.compareID.Visible = false;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 134);
            this.Controls.Add(this.compareID);
            this.Controls.Add(this.confirmDeleteButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IDText);
            this.Name = "Form4";
            this.Text = "Form4";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox IDText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button confirmDeleteButton;
        private System.Windows.Forms.TextBox compareID;
    }
}