﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Employee_Form
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void insertButton_Click(object sender, EventArgs e)
        {
            bool canContinue = checkTextBoxes();
            if (!canContinue)
                return;
            canContinue = checkCheckBoxes();
            if (!canContinue)
                return;

            string firstString = "", middleString = "", lastString = "", genderString = "", ageString = "", IDString = "";
            if (!string.IsNullOrEmpty(fnameText.Text))
                firstString = fnameText.Text;
            if (!string.IsNullOrEmpty(mnameText.Text))
                middleString = mnameText.Text;
            if (!string.IsNullOrEmpty(lnameText.Text))
                lastString = lnameText.Text;
            if (!string.IsNullOrEmpty(genderText.Text))
                genderString = getGenderChar(genderText.Text);
            if (!string.IsNullOrEmpty(ageText.Text))
                ageString = ageText.Text;
            if (!string.IsNullOrEmpty(IDText.Text))
                IDString = IDText.Text;

            bool first = firstCheck.Checked;
            bool middle = middleCheck.Checked;
            bool last = lastCheck.Checked;
            bool gender = genderCheck.Checked;
            bool age = ageCheck.Checked;
            bool id = IDCheck.Checked;

            bool[] boolValues = new bool[] { first, middle, last, gender, age, id };

            string string1 = getSQL1(first, middle, last, gender, age, id);
            string string2 = getSQL2(firstString, middleString, lastString, genderString, ageString, IDString);
            string query = string1 + string2;

            Form6 displayForm = new Form6(query, boolValues);
            displayForm.Show();
        }

        private string getGenderChar(string gender)
        {
            string result;

            if (gender == "Male")
                return "M";
            else if (gender == "Female")
                return "F";
            else
                return "O";
        }

        private bool checkTextBoxes()
        {
            if (string.IsNullOrEmpty(fnameText.Text) && string.IsNullOrEmpty(mnameText.Text) && string.IsNullOrEmpty(lnameText.Text)
                && string.IsNullOrEmpty(genderText.Text) && string.IsNullOrEmpty(ageText.Text) && string.IsNullOrEmpty(IDText.Text))
            {
                MessageBox.Show("At least one field must be filled in.");
                return false;
            }

            if (!string.IsNullOrEmpty(ageText.Text))
            {
                bool ageCheck = int.TryParse(ageText.Text, out int m);
                if(!ageCheck)
                {
                    MessageBox.Show("The Age field must be an integer.");
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(IDText.Text))
            {
                bool IDCheck = int.TryParse(IDText.Text, out int n);
                if(!IDCheck)
                {
                    MessageBox.Show("The ID field must be an integer.");
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(genderText.Text))
            {
                if (genderText.Text != "Male" && genderText.Text != "Female" && genderText.Text != "Other")
                {
                    MessageBox.Show("Please select a gender from the list.");
                    return false;
                }
            }
            
            return true;
        }

        private bool checkCheckBoxes()
        {
            if (!firstCheck.Checked && !middleCheck.Checked && !lastCheck.Checked && !genderCheck.Checked && !ageCheck.Checked && !IDCheck.Checked)
            {
                MessageBox.Show("At least one display option must be selected.");
                return false;
            }
            
            return true;
        }

        private void genderText_MouseClick(object sender, MouseEventArgs e)
        {
            genderText.DroppedDown = true;
        }

        private string getSQL1(bool first, bool middle, bool last, bool gender, bool age, bool ID)
        {
            bool needsComma = false;
            string result = "SELECT ";
            string addition = "";

            if (first)
            {
                addition = "First_Name";
                needsComma = true;
                result = result + addition;
            }

            if (middle)
            {
                if (needsComma)
                {
                    addition = ", Middle_Name";
                    result = result + addition;
                }
                else
                {
                    addition = "Middle_Name";
                    needsComma = true;
                    result = result + addition;
                }
            }

            if (last)
            {
                if (needsComma)
                {
                    addition = ", Last_Name";
                    result = result + addition;
                }
                else
                {
                    addition = "Last_Name";
                    needsComma = true;
                    result = result + addition;
                }
            }

            if (gender)
            {
                if (needsComma)
                {
                    addition = ", Gender";
                    result = result + addition;
                }
                else
                {
                    addition = "Gender";
                    needsComma = true;
                    result = result + addition;
                }
            }

            if (age)
            {
                if (needsComma)
                {
                    addition = ", Age";
                    result = result + addition;
                }
                else
                {
                    addition = "Age";
                    needsComma = true;
                    result = result + addition;
                }
            }

            if (ID)
            {
                if (needsComma)
                {
                    addition = ", ID";
                    result = result + addition;
                }
                else
                {
                    addition = "ID";
                    result = result + addition;
                }
            }

            return result;
        }

        private string getSQL2(string first, string middle, string last, string gender, string age, string ID)
        {
            bool needsComma = false;
            string result = " FROM Employee WHERE ";
            string addition = "";

            if (first != "")
            {
                addition = "First_Name" +  "\'" + first + "\'";
                needsComma = true;
                result = result + addition;
            }

            if (middle != "")
            {
                if(needsComma)
                {
                    addition = ", Middle_Name = " + "\'" + middle+ "\'";
                    result = result + addition;
                }
                else
                {
                    addition = "Middle_Name = " + "\'" + middle + "\'";
                    needsComma = true;
                    result = result + addition;
                }
            }

            if (last != "")
            {
                if (needsComma)
                {
                    addition = ", Last_Name = " + "\'" + last + "\'";
                    result = result + addition;
                }
                else
                {
                    addition = "Last_Name = " + "\'" + last + "\'";
                    needsComma = true;
                    result = result + addition;
                }
            }

            if (gender != "")
            {
                if (needsComma)
                {
                    addition = ", Gender = " + "\'" + gender + "\'";
                    result = result + addition;
                }
                else
                {
                    addition = "Gender = " + "\'" + gender + "\'";
                    needsComma = true;
                    result = result + addition;
                }
            }

            if (age != "")
            {
                if (needsComma)
                {
                    addition = ", Age = " + "\'" + age + "\'";
                    result = result + addition;
                }
                else
                {
                    addition = "Age = " + "\'" + age + "\'";
                    needsComma = true;
                    result = result + addition;
                }
            }

            if (ID != "")
            {
                if (needsComma)
                {
                    addition = ", ID = " + "\'" + ID + "\'";
                    result = result + addition;
                }
                else
                {
                    addition = "ID = " + "\'" + ID + "\'";
                    result = result + addition;
                }
            }

            return result;
        }
    }
}
