﻿
namespace Employee_Form
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.genderText = new System.Windows.Forms.ComboBox();
            this.lnameText = new System.Windows.Forms.TextBox();
            this.ageText = new System.Windows.Forms.TextBox();
            this.mnameText = new System.Windows.Forms.TextBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.fnameText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // genderText
            // 
            this.genderText.FormattingEnabled = true;
            this.genderText.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Other"});
            this.genderText.Location = new System.Drawing.Point(36, 210);
            this.genderText.Name = "genderText";
            this.genderText.Size = new System.Drawing.Size(204, 21);
            this.genderText.TabIndex = 28;
            this.genderText.MouseClick += new System.Windows.Forms.MouseEventHandler(this.genderText_MouseClick);
            // 
            // lnameText
            // 
            this.lnameText.Location = new System.Drawing.Point(36, 156);
            this.lnameText.Name = "lnameText";
            this.lnameText.Size = new System.Drawing.Size(204, 20);
            this.lnameText.TabIndex = 27;
            // 
            // ageText
            // 
            this.ageText.Location = new System.Drawing.Point(36, 264);
            this.ageText.Name = "ageText";
            this.ageText.Size = new System.Drawing.Size(204, 20);
            this.ageText.TabIndex = 25;
            // 
            // mnameText
            // 
            this.mnameText.Location = new System.Drawing.Point(36, 98);
            this.mnameText.Name = "mnameText";
            this.mnameText.Size = new System.Drawing.Size(204, 20);
            this.mnameText.TabIndex = 24;
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(87, 304);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(96, 23);
            this.deleteButton.TabIndex = 23;
            this.deleteButton.Text = "Delete Employee";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // fnameText
            // 
            this.fnameText.Location = new System.Drawing.Point(36, 49);
            this.fnameText.Name = "fnameText";
            this.fnameText.Size = new System.Drawing.Size(204, 20);
            this.fnameText.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Gender";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 248);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Age";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Last Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Middle Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "First Name";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 358);
            this.Controls.Add(this.genderText);
            this.Controls.Add(this.lnameText);
            this.Controls.Add(this.ageText);
            this.Controls.Add(this.mnameText);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.fnameText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox genderText;
        private System.Windows.Forms.TextBox lnameText;
        private System.Windows.Forms.TextBox ageText;
        private System.Windows.Forms.TextBox mnameText;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.TextBox fnameText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}