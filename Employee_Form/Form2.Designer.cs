﻿
namespace Employee_Form
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.fnameText = new System.Windows.Forms.TextBox();
            this.insertButton = new System.Windows.Forms.Button();
            this.mnameText = new System.Windows.Forms.TextBox();
            this.ageText = new System.Windows.Forms.TextBox();
            this.IDText = new System.Windows.Forms.TextBox();
            this.lnameText = new System.Windows.Forms.TextBox();
            this.genderText = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "First Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Middle Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Last Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 290);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Employee ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Age";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 181);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Gender";
            // 
            // fnameText
            // 
            this.fnameText.Location = new System.Drawing.Point(29, 36);
            this.fnameText.Name = "fnameText";
            this.fnameText.Size = new System.Drawing.Size(204, 20);
            this.fnameText.TabIndex = 8;
            // 
            // insertButton
            // 
            this.insertButton.Location = new System.Drawing.Point(80, 365);
            this.insertButton.Name = "insertButton";
            this.insertButton.Size = new System.Drawing.Size(96, 23);
            this.insertButton.TabIndex = 9;
            this.insertButton.Text = "Insert Employee";
            this.insertButton.UseVisualStyleBackColor = true;
            this.insertButton.Click += new System.EventHandler(this.insertButton_Click);
            // 
            // mnameText
            // 
            this.mnameText.Location = new System.Drawing.Point(29, 85);
            this.mnameText.Name = "mnameText";
            this.mnameText.Size = new System.Drawing.Size(204, 20);
            this.mnameText.TabIndex = 10;
            // 
            // ageText
            // 
            this.ageText.Location = new System.Drawing.Point(29, 251);
            this.ageText.Name = "ageText";
            this.ageText.Size = new System.Drawing.Size(204, 20);
            this.ageText.TabIndex = 12;
            // 
            // IDText
            // 
            this.IDText.Location = new System.Drawing.Point(29, 310);
            this.IDText.Name = "IDText";
            this.IDText.Size = new System.Drawing.Size(204, 20);
            this.IDText.TabIndex = 13;
            // 
            // lnameText
            // 
            this.lnameText.Location = new System.Drawing.Point(29, 143);
            this.lnameText.Name = "lnameText";
            this.lnameText.Size = new System.Drawing.Size(204, 20);
            this.lnameText.TabIndex = 14;
            // 
            // genderText
            // 
            this.genderText.FormattingEnabled = true;
            this.genderText.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Other"});
            this.genderText.Location = new System.Drawing.Point(29, 197);
            this.genderText.Name = "genderText";
            this.genderText.Size = new System.Drawing.Size(204, 21);
            this.genderText.TabIndex = 15;
            this.genderText.MouseClick += new System.Windows.Forms.MouseEventHandler(this.genderText_MouseClick);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 420);
            this.Controls.Add(this.genderText);
            this.Controls.Add(this.lnameText);
            this.Controls.Add(this.IDText);
            this.Controls.Add(this.ageText);
            this.Controls.Add(this.mnameText);
            this.Controls.Add(this.insertButton);
            this.Controls.Add(this.fnameText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox fnameText;
        private System.Windows.Forms.Button insertButton;
        private System.Windows.Forms.TextBox mnameText;
        private System.Windows.Forms.TextBox ageText;
        private System.Windows.Forms.TextBox IDText;
        private System.Windows.Forms.TextBox lnameText;
        private System.Windows.Forms.ComboBox genderText;
    }
}