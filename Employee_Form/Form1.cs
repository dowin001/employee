﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Employee_Form
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void insertButton_Click(object sender, EventArgs e)
        {
            Form2 insertForm = new Form2();
            insertForm.Show();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            Form3 deleteForm = new Form3();
            deleteForm.Show();
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            Form5 viewForm = new Form5();
            viewForm.Show();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            Form7 editForm = new Form7();
            editForm.Show();
        }
    }
}
