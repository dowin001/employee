﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace Employee_Form
{
    public partial class Form6 : Form
    {

        string conString = ConfigurationManager.ConnectionStrings["Employee_Form.Properties.Settings.Employee_ViewConnectionString"].ConnectionString;
        
        public Form6(string query, bool[] boolValues)
        {
            InitializeComponent();
            displayResults(query, boolValues);
        }

        private void displayResults(string SQLQuery, bool[] boolValues)
        {
            SqlConnection con = new SqlConnection(conString);
            con.Open();

            string query = SQLQuery;
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
                row.Cells[0].Value = getRow(reader, boolValues);
                dataGridView1.Rows.Add(row);
            }
        }

        private string getRow(SqlDataReader reader, bool[] boolValues)
        {
            string result = "";

            if(boolValues[0])
            {
                result = result + Convert.ToString(reader["First_Name"]);
            }

            if(boolValues[1])
            {
                if (result != "")
                    result = result + " ";
                result = result + Convert.ToString(reader["Middle_Name"]);
            }

            if(boolValues[2])
            {
                if (result != "")
                    result = result + " ";
                result = result + Convert.ToString(reader["Last_Name"]);
            }

            if(boolValues[3])
            {
                if (result != "")
                    result = result + ", ";
                
                result = result + Convert.ToString(reader["Gender"]);
            }

            if(boolValues[4])
            {
                if (result != "")
                    result = result + ", ";

                result = result + Convert.ToString(reader["Age"]);
            }

            if (boolValues[5])
            {
                if (result != "")
                    result = result + ", ";

                result = result + Convert.ToString(reader["ID"]);
            }

            return result;
        }
    }
}
