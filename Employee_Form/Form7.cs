﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace Employee_Form
{
    public partial class Form7 : Form
    {
        string conString = ConfigurationManager.ConnectionStrings["Employee_Form.Properties.Settings.Employee_ViewConnectionString"].ConnectionString;

        public Form7()
        {
            InitializeComponent();
        }

        private void confirmEditButton_Click(object sender, EventArgs e)
        {
            bool IDCheck = int.TryParse(IDText.Text, out int m);

            if (IDCheck)
                doSQL();
            else
                MessageBox.Show("Enter a valid ID number.");
        }

        private void doSQL()
        {
            string query = "SELECT ID FROM Employee WHERE ID = @ID";
            SqlConnection con = new SqlConnection(conString);
            con.Open();

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Convert.ToInt32(IDText.Text);

            SqlDataReader read = cmd.ExecuteReader();
            if (read.HasRows)
            {
                Form8 editForm = new Form8(Convert.ToInt32(IDText.Text));
                editForm.Show();
            }
            else
            {
                MessageBox.Show("No employee with that ID number exists.");
            }
        }
    }
}
