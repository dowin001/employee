﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace Employee_Form
{
    public partial class Form3 : Form
    {

        string conString = ConfigurationManager.ConnectionStrings["Employee_Form.Properties.Settings.Employee_ViewConnectionString"].ConnectionString;
        public Form3()
        {
            InitializeComponent();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            bool canContinue = checkData();
            if (!canContinue)
                return;

            string fname = fnameText.Text;
            string mname = mnameText.Text;
            string lname = lnameText.Text;
            char gender;
            int age = Convert.ToInt32(ageText.Text);

            if (genderText.Text == "Male")
            {
                gender = 'M';
            }
            else if (genderText.Text == "Female")
            {
                gender = 'F';
            }
            else
            {
                gender = 'O';
            }

            string query = "SELECT ID FROM Employee WHERE First_Name = @fname AND Middle_Name = @mname AND Last_Name = @lname AND Gender = @gender AND Age = @age;";
            SqlConnection con = new SqlConnection(conString);
            con.Open();

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50).Value = fname;
            cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = mname;
            cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = lname;
            cmd.Parameters.Add("@gender", SqlDbType.Char).Value = gender;
            cmd.Parameters.Add("@age", SqlDbType.Int).Value = age;

            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                Form4 confirmDelete = new Form4(Convert.ToInt32(reader["ID"]));
                confirmDelete.Show();
            }
            else
            {
                MessageBox.Show("That employee does not exist.");
            }

            con.Close();

        }

        private bool checkData()
        {
            if (string.IsNullOrEmpty(fnameText.Text) || string.IsNullOrEmpty(mnameText.Text) || string.IsNullOrEmpty(lnameText.Text)
                || string.IsNullOrEmpty(genderText.Text) || string.IsNullOrEmpty(ageText.Text))
            {
                MessageBox.Show("All fields must be filled in.");
                return false;
            }

            bool ageCheck = int.TryParse(ageText.Text, out int m);

            if (!ageCheck)
            {
                MessageBox.Show("The Age field must be an integer.");
                return false;
            }

            if (genderText.Text != "Male" && genderText.Text != "Female" && genderText.Text != "Other")
            {
                MessageBox.Show("Please select a gender from the list.");
                return false;
            }

            return true;
        }

        private void genderText_MouseClick(object sender, MouseEventArgs e)
        {
            genderText.DroppedDown = true;
        }
    }
}
