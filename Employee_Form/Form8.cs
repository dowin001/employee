﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace Employee_Form
{
    public partial class Form8 : Form
    {
        string conString = ConfigurationManager.ConnectionStrings["Employee_Form.Properties.Settings.Employee_ViewConnectionString"].ConnectionString;

        public Form8(int ID)
        {
            InitializeComponent();
            IDText.Text = Convert.ToString(ID);
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            bool canContinue = checkTextBoxes();
            if (!canContinue)
                return;

            string query = "UPDATE Employee SET First_Name = @fname, Middle_Name = @mname, Last_Name = @lname, Gender = @gender, Age = @age WHERE ID = @ID";
            char genderChar = getGenderChar(genderText.Text);

            SqlConnection con = new SqlConnection(conString);
            con.Open();

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50).Value = fnameText.Text;
            cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = mnameText.Text;
            cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = lnameText.Text;
            cmd.Parameters.Add("@gender", SqlDbType.Char, 1).Value = genderChar;
            cmd.Parameters.Add("@Age", SqlDbType.Int).Value = Convert.ToInt32(ageText.Text);
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Convert.ToInt32(IDText.Text);

            cmd.ExecuteNonQuery();
            MessageBox.Show("Employee information has been updated.");
            this.Close();
        }

        private char getGenderChar(string gender)
        {
            if (gender == "Male")
                return 'M';
            else if (gender == "Female")
                return 'F';
            else
                return 'O';
        }

        private bool checkTextBoxes()
        {
            if (string.IsNullOrEmpty(fnameText.Text) || string.IsNullOrEmpty(mnameText.Text) || string.IsNullOrEmpty(lnameText.Text)
                || string.IsNullOrEmpty(genderText.Text) || string.IsNullOrEmpty(ageText.Text))
            {
                MessageBox.Show("Please fill in all fields.");
                return false;
            }

            if (!string.IsNullOrEmpty(ageText.Text))
            {
                bool ageCheck = int.TryParse(ageText.Text, out int m);
                if (!ageCheck)
                {
                    MessageBox.Show("The Age field must be an integer.");
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(genderText.Text))
            {
                if (genderText.Text != "Male" && genderText.Text != "Female" && genderText.Text != "Other")
                {
                    MessageBox.Show("Please select a gender from the list.");
                    return false;
                }
            }

            return true;
        }

        private void genderText_MouseClick(object sender, MouseEventArgs e)
        {
            genderText.DroppedDown = true;
        }
    }
}
