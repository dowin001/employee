﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace Employee_Form
{
    public partial class Form4 : Form
    {
        string conString = ConfigurationManager.ConnectionStrings["Employee_Form.Properties.Settings.Employee_ViewConnectionString"].ConnectionString;
        public Form4(int testID)
        {
            InitializeComponent();
            compareID.Text = Convert.ToString(testID);
        }

        private void confirmDeleteButton_Click(object sender, EventArgs e)
        {
            if (IDText.Text == compareID.Text)
            {
                string query = "DELETE FROM Employee WHERE ID = @ID";
                SqlConnection con = new SqlConnection(conString);
                con.Open();

                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = compareID.Text;
                cmd.ExecuteNonQuery();

                MessageBox.Show("Employee deleted.");
                this.Close();
            }
            else
                MessageBox.Show("That ID does not match the database.");
        }
    }
}
