﻿
namespace Employee_Form
{
    partial class Form7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.confirmEditButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.IDText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // confirmEditButton
            // 
            this.confirmEditButton.Location = new System.Drawing.Point(115, 99);
            this.confirmEditButton.Name = "confirmEditButton";
            this.confirmEditButton.Size = new System.Drawing.Size(75, 23);
            this.confirmEditButton.TabIndex = 6;
            this.confirmEditButton.Text = "Edit";
            this.confirmEditButton.UseVisualStyleBackColor = true;
            this.confirmEditButton.Click += new System.EventHandler(this.confirmEditButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(100, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Employee ID number:";
            // 
            // IDText
            // 
            this.IDText.Location = new System.Drawing.Point(103, 69);
            this.IDText.Name = "IDText";
            this.IDText.Size = new System.Drawing.Size(100, 20);
            this.IDText.TabIndex = 4;
            // 
            // Form7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 134);
            this.Controls.Add(this.confirmEditButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IDText);
            this.Name = "Form7";
            this.Text = "Form7";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button confirmEditButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox IDText;
    }
}