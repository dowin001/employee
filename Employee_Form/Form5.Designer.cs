﻿
namespace Employee_Form
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.genderText = new System.Windows.Forms.ComboBox();
            this.lnameText = new System.Windows.Forms.TextBox();
            this.IDText = new System.Windows.Forms.TextBox();
            this.ageText = new System.Windows.Forms.TextBox();
            this.mnameText = new System.Windows.Forms.TextBox();
            this.insertButton = new System.Windows.Forms.Button();
            this.fnameText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.firstCheck = new System.Windows.Forms.CheckBox();
            this.middleCheck = new System.Windows.Forms.CheckBox();
            this.lastCheck = new System.Windows.Forms.CheckBox();
            this.genderCheck = new System.Windows.Forms.CheckBox();
            this.ageCheck = new System.Windows.Forms.CheckBox();
            this.IDCheck = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // genderText
            // 
            this.genderText.FormattingEnabled = true;
            this.genderText.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Other"});
            this.genderText.Location = new System.Drawing.Point(27, 224);
            this.genderText.Name = "genderText";
            this.genderText.Size = new System.Drawing.Size(204, 21);
            this.genderText.TabIndex = 28;
            this.genderText.MouseClick += new System.Windows.Forms.MouseEventHandler(this.genderText_MouseClick);
            // 
            // lnameText
            // 
            this.lnameText.Location = new System.Drawing.Point(27, 170);
            this.lnameText.Name = "lnameText";
            this.lnameText.Size = new System.Drawing.Size(204, 20);
            this.lnameText.TabIndex = 27;
            // 
            // IDText
            // 
            this.IDText.Location = new System.Drawing.Point(27, 337);
            this.IDText.Name = "IDText";
            this.IDText.Size = new System.Drawing.Size(204, 20);
            this.IDText.TabIndex = 26;
            // 
            // ageText
            // 
            this.ageText.Location = new System.Drawing.Point(27, 278);
            this.ageText.Name = "ageText";
            this.ageText.Size = new System.Drawing.Size(204, 20);
            this.ageText.TabIndex = 25;
            // 
            // mnameText
            // 
            this.mnameText.Location = new System.Drawing.Point(27, 112);
            this.mnameText.Name = "mnameText";
            this.mnameText.Size = new System.Drawing.Size(204, 20);
            this.mnameText.TabIndex = 24;
            // 
            // insertButton
            // 
            this.insertButton.Location = new System.Drawing.Point(68, 561);
            this.insertButton.Name = "insertButton";
            this.insertButton.Size = new System.Drawing.Size(96, 23);
            this.insertButton.TabIndex = 23;
            this.insertButton.Text = "Search";
            this.insertButton.UseVisualStyleBackColor = true;
            this.insertButton.Click += new System.EventHandler(this.insertButton_Click);
            // 
            // fnameText
            // 
            this.fnameText.Location = new System.Drawing.Point(27, 63);
            this.fnameText.Name = "fnameText";
            this.fnameText.Size = new System.Drawing.Size(204, 20);
            this.fnameText.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 208);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Gender";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 262);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Age";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 317);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Employee ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Last Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Middle Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "First Name";
            // 
            // firstCheck
            // 
            this.firstCheck.AutoSize = true;
            this.firstCheck.Location = new System.Drawing.Point(84, 400);
            this.firstCheck.Name = "firstCheck";
            this.firstCheck.Size = new System.Drawing.Size(76, 17);
            this.firstCheck.TabIndex = 29;
            this.firstCheck.Text = "First Name";
            this.firstCheck.UseVisualStyleBackColor = true;
            // 
            // middleCheck
            // 
            this.middleCheck.AutoSize = true;
            this.middleCheck.Location = new System.Drawing.Point(84, 423);
            this.middleCheck.Name = "middleCheck";
            this.middleCheck.Size = new System.Drawing.Size(88, 17);
            this.middleCheck.TabIndex = 30;
            this.middleCheck.Text = "Middle Name";
            this.middleCheck.UseVisualStyleBackColor = true;
            // 
            // lastCheck
            // 
            this.lastCheck.AutoSize = true;
            this.lastCheck.Location = new System.Drawing.Point(84, 446);
            this.lastCheck.Name = "lastCheck";
            this.lastCheck.Size = new System.Drawing.Size(77, 17);
            this.lastCheck.TabIndex = 31;
            this.lastCheck.Text = "Last Name";
            this.lastCheck.UseVisualStyleBackColor = true;
            // 
            // genderCheck
            // 
            this.genderCheck.AutoSize = true;
            this.genderCheck.Location = new System.Drawing.Point(84, 469);
            this.genderCheck.Name = "genderCheck";
            this.genderCheck.Size = new System.Drawing.Size(61, 17);
            this.genderCheck.TabIndex = 32;
            this.genderCheck.Text = "Gender";
            this.genderCheck.UseVisualStyleBackColor = true;
            // 
            // ageCheck
            // 
            this.ageCheck.AutoSize = true;
            this.ageCheck.Location = new System.Drawing.Point(84, 492);
            this.ageCheck.Name = "ageCheck";
            this.ageCheck.Size = new System.Drawing.Size(45, 17);
            this.ageCheck.TabIndex = 33;
            this.ageCheck.Text = "Age";
            this.ageCheck.UseVisualStyleBackColor = true;
            // 
            // IDCheck
            // 
            this.IDCheck.AutoSize = true;
            this.IDCheck.Location = new System.Drawing.Point(84, 515);
            this.IDCheck.Name = "IDCheck";
            this.IDCheck.Size = new System.Drawing.Size(37, 17);
            this.IDCheck.TabIndex = 34;
            this.IDCheck.Text = "ID";
            this.IDCheck.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 384);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "Display Options:";
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 596);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.IDCheck);
            this.Controls.Add(this.ageCheck);
            this.Controls.Add(this.genderCheck);
            this.Controls.Add(this.lastCheck);
            this.Controls.Add(this.middleCheck);
            this.Controls.Add(this.firstCheck);
            this.Controls.Add(this.genderText);
            this.Controls.Add(this.lnameText);
            this.Controls.Add(this.IDText);
            this.Controls.Add(this.ageText);
            this.Controls.Add(this.mnameText);
            this.Controls.Add(this.insertButton);
            this.Controls.Add(this.fnameText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form5";
            this.Text = "Form5";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox genderText;
        private System.Windows.Forms.TextBox lnameText;
        private System.Windows.Forms.TextBox IDText;
        private System.Windows.Forms.TextBox ageText;
        private System.Windows.Forms.TextBox mnameText;
        private System.Windows.Forms.Button insertButton;
        private System.Windows.Forms.TextBox fnameText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox firstCheck;
        private System.Windows.Forms.CheckBox middleCheck;
        private System.Windows.Forms.CheckBox lastCheck;
        private System.Windows.Forms.CheckBox genderCheck;
        private System.Windows.Forms.CheckBox ageCheck;
        private System.Windows.Forms.CheckBox IDCheck;
        private System.Windows.Forms.Label label4;
    }
}