﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace Employee_Form
{
    public partial class Form2 : Form
    {
        string conString = ConfigurationManager.ConnectionStrings["Employee_Form.Properties.Settings.Employee_ViewConnectionString"].ConnectionString;
        
        public Form2()
        {
            InitializeComponent();
        }

        private void genderText_MouseClick(object sender, MouseEventArgs e)
        {
            genderText.DroppedDown = true;
        }

        private void insertButton_Click(object sender, EventArgs e)
        {
            bool canContinue = checkData();
            if (!canContinue)
                return;
            
            string fname = fnameText.Text;
            string mname = mnameText.Text;
            string lname = lnameText.Text;
            char gender;
            int age = Convert.ToInt32(ageText.Text);
            int ID = Convert.ToInt32(IDText.Text);

            if (genderText.Text == "Male")
            {
                gender = 'M';
            }
            else if (genderText.Text == "Female")
            {
                gender = 'F';
            }
            else
            {
                gender = 'O';
            }
            
            SqlConnection con = new SqlConnection(conString);
            con.Open();

            string query = "INSERT INTO Employee (First_Name, Middle_Name, Last_Name, Gender, Age, ID) " +
                           "VALUES ('" + fname + "', '" + mname + "', '" + lname + "', '" + gender + "', '" + age + "', '" + ID + "');";

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();

            con.Close();

            MessageBox.Show("Employee successfully inserted into database.");

            this.Close();
        }

        private bool checkData()
        {
            if (string.IsNullOrEmpty(fnameText.Text) || string.IsNullOrEmpty(mnameText.Text) || string.IsNullOrEmpty(lnameText.Text)
                || string.IsNullOrEmpty(genderText.Text) || string.IsNullOrEmpty(ageText.Text) || string.IsNullOrEmpty(IDText.Text))
            {
                MessageBox.Show("All fields must be filled in.");
                return false;
            }

            bool ageCheck = int.TryParse(ageText.Text, out int m);
            bool IDCheck = int.TryParse(IDText.Text, out int n);

            if (!ageCheck || !IDCheck)
            {
                MessageBox.Show("The Age and ID fields must be integers.");
                return false;
            }

            if (genderText.Text != "Male" && genderText.Text != "Female" && genderText.Text != "Other")
            {
                MessageBox.Show("Please select a gender from the list.");
                return false;
            }

            SqlConnection con = new SqlConnection(conString);
            con.Open();

            string query = "SELECT ID FROM Employee;";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                int testID = Convert.ToInt32(reader["ID"]);
                if (Convert.ToInt32(IDText.Text) == testID)
                {
                    MessageBox.Show("The selected ID is already in use.");
                    return false;
                }
            }

            return true;
        }
    }
}
